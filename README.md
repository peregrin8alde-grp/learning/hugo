# hugo

[静的サイトジェネレーター HUGO](https://gohugo.io/) の学習用

- 自作サイトごとにブランチを作成することで、１プロジェクト内で色々作成したい。
  - GitLab Pages を使う場合は main ブランチじゃないとダメ（pages 作成自体はどのブランチでもできそうだが、ブランチごとに pages を分けることができないため、１つのサイトとして統合しないとどれか１つのサイトしか表示できない）？
    - 正式なサイトなら元々１つなので問題ないはず。練習用としては編集中のものだけ pages にできれば良いものとする。
  - 最終的に１つにマージする場合を考慮して、サイト自体はディレクトリを分ける。
    - ブランチ名のプレフィックスにサイト名付与し、ブランチ名からサイト名を切り出してビルド対象ディレクトリを決める。
    - 各サイトの修正時のブランチ名は「サイト名-issue番号-動作-対象」とする。  
      例：`mysite01/2-init-site`、`mysite01/3-add-contents-doc01`
    - できれば各サイト用ブランチのベース自体は他サイトが存在しない状態にしたい
      - main へのマージ自体は `git checkout ブランチ名 ファイル名` でブランチ間のファイルコピーすれば簡単なはず
      - ブランチ作成後の初期化作業として sites ディレクトリ配下全削除するしかないか？
      - 他サイトも残しておいた方がMRでの差分チェックや他サイト流用などができて便利なので、残すことにする
  - テーマ用のサブモジュールの扱いがややこしそう？
    - 公開用サイトのディレクトリ構成として site ディレクトリ配下に１つだけ themes を用意、そこにサブモジュールを集めることにする
    - 各サイトはそこに対してシンボリックリンクを作成する
      ```
      git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke
      git submodule add https://github.com/google/docsy.git themes/docsy
      git submodule update --init --recursive

      cd sites/quickstart/themes
      ln -s ../../../themes/ananke ananke

      cd sites/docsy/themes
      ln -s ../../../themes/docsy docsy
      ```

## install

https://gohugo.io/getting-started/installing/#binary-cross-platform

```
curl -LO https://github.com/gohugoio/hugo/releases/download/v0.88.1/hugo_extended_0.88.1_Linux-64bit.tar.gz
sudo tar zxvf hugo_extended_0.88.1_Linux-64bit.tar.gz -C /usr/local/bin hugo
rm -rf hugo_extended_0.88.1_Linux-64bit.tar.gz

hugo version
```

## Quick Start

https://gohugo.io/getting-started/quick-start/

```
hugo new site sites/quickstart

cd sites/quickstart
git submodule add https://github.com/theNewDynamic/gohugo-theme-ananke.git themes/ananke

echo theme = \"ananke\" >> config.toml

hugo new posts/my-first-post.md

###sed -i -e "s/^draft: .*/draft: false/g" content/posts/my-first-post.md
echo test >> content/posts/my-first-post.md

# デプロイ
## -D で draft もデプロイ
hugo server -D

# ビルド
hugo -D
```

## asciidoc

参考：
- https://hub.docker.com/r/klakegg/hugo/
  - `asciidoctor-diagram` のような拡張が入っていない？
    - そもそも画像は別途作成して Asciidoctor の `data-uri` 指定で埋め込みたいので使わない？
    - ただ、他の拡張なども使うことがありえるため、 Asciidoctor 環境に Hugo を持っていく方向で考える。
      - なお、 `asciidoctor/docker-asciidoctor` イメージは alpine のため `hugo` バイナリをそのまま持っていくことはできなさそう。
      - `mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2` で上記問題は解決可能？
- https://gohugo.io/content-management/formats/#external-helpers
- https://gohugo.io/content-management/formats/#external-helper-asciidoctor


```
hugo new posts/test-asciidoc.adoc
```

`asciidoctor` をインストールする代わりに `asciidoctor` コンテナ起動のラッパーを作成する場合

```
sudo tee /usr/local/bin/asciidoctor <<'EOF' >> /dev/null
#!/bin/sh
if [ -p /dev/stdin ]; then
  cat - | docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    asciidoctor/docker-asciidoctor \
      asciidoctor "$@"
else
  docker run \
    --rm \
    -u $(id -u):$(id -g) \
    -i \
    -v "$(pwd):$(pwd)" \
    -w "$(pwd)" \
    -e TZ=Asia/Tokyo \
    asciidoctor/docker-asciidoctor \
      asciidoctor "$@"
fi
EOF
sudo chmod a+x /usr/local/bin/asciidoctor

hugo server -D
```

`asciidoctor` をインストールする代わりに `asciidoctor` コンテナ上で実行する場合

```
docker run \
  --rm \
  -p 1313:1313 \
  -v "$(pwd):$(pwd)" \
  -v "/usr/local/bin/hugo:/usr/local/bin/hugo" \
  -e TZ=Asia/Tokyo \
  asciidoctor/docker-asciidoctor \
    sh -c "mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 \
      && /usr/local/bin/hugo server --bind 0.0.0.0 -D"

sudo rm -rf public/*
docker run \
  --rm \
  -v "$(pwd):$(pwd)" \
  -v "/usr/local/bin/hugo:/usr/local/bin/hugo" \
  -e TZ=Asia/Tokyo \
  asciidoctor/docker-asciidoctor \
    sh -c "mkdir /lib64 && ln -s /lib/libc.musl-x86_64.so.1 /lib64/ld-linux-x86-64.so.2 \
      && /usr/local/bin/hugo -D"
```

Asciidoctor 単独での動作確認

- front matter はスキップ
  - https://docs.asciidoctor.org/asciidoctor/latest/html-backend/skip-front-matter/


```
rm -rf $(pwd)/_build
docker run \
  -u $(id -u):$(id -g) \
  --rm \
  -v "$(pwd):$(pwd)" \
  -e TZ=Asia/Tokyo \
  asciidoctor/docker-asciidoctor \
    asciidoctor \
    -D $(pwd)/_build \
    -R ./content \
    -a skip-front-matter \
    -a data-uri \
    "**/*.adoc"
```
