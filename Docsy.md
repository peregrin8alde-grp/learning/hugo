# Docsy

- https://www.docsy.dev/
- https://github.com/google/docsy

技術文書向けテーマ

## install

```
hugo new site sites/docsy
cd sites/docsy

git init
git submodule add https://github.com/google/docsy.git themes/docsy
echo 'theme = "docsy"' >> config.toml
git submodule update --init --recursive
```

ビルドには PostCSS も必須？

```
sudo npm install -g autoprefixer
sudo npm install -g postcss-cli
sudo npm install -g postcss
```

`PostCSS` をインストールする代わりに `node` コンテナ起動のラッパーを作成する場合

```
sudo tee /usr/local/bin/npm <<'EOF' >> /dev/null
#!/bin/sh

docker run \
  --rm \
  -v npm_modules:/usr/local/lib/node_modules \
  -v "$(pwd):$(pwd)" \
  -w $(pwd) \
  node \
    npm "$@"
EOF
sudo chmod a+x /usr/local/bin/npm

# postcss を実行するためには入力パイプ処理を扱えないといけない
# `#!/usr/bin/env node` で実行されるため、 alias では無理
sudo tee /usr/local/bin/node <<'EOF' >> /dev/null
#!/bin/sh

if [ -p /dev/stdin ]; then
  cat - | docker run \
    --rm \
    -i \
    -v npm_modules:/usr/local/lib/node_modules \
    -v "$(pwd):$(pwd)" \
    -w $(pwd) \
    node \
      node "$@"
else
  docker run \
    --rm \
    -i \
    -v npm_modules:/usr/local/lib/node_modules \
    -v "$(pwd):$(pwd)" \
    -w $(pwd) \
    node \
      node "$@"
fi
EOF
sudo chmod a+x /usr/local/bin/node

npm install -g autoprefixer
npm install -g postcss-cli
npm install -g postcss

sudo hugo -D
```

## example

- https://www.docsy.dev/docs/getting-started/#using-the-command-line
- 技術文書向けの構成になっている。
  - Overview
  - Getting Started
  - Examples
  - Concepts
  - Core Tasks
  - Tutorials
  - Reference
  - Contribution Guidelines

git の扱いがややこしくなるため、一から作成したサイトに必要なものだけコピーする。

```
cd sites/docsy

git clone --depth 1 https://github.com/google/docsy-example.git

cp -rf docsy-example/{content,layouts,config.toml} ./
rm -rf docsy-example

sed -i -e 's|baseURL = "/"|baseURL = "http://localhost/"|g' config.toml

hugo server
```

## 日本語検索

参考：
- https://github.com/MihaiValentin/lunr-languages
- https://www.jsdelivr.com/package/npm/lunr-languages
- https://progrhy.me/tech-notes/a/memo/2020/20200515/

テーマ修正：
- `themes/docsy/assets/js/offline-search.js` を `assets/js/offline-search.js` にコピーして修正
  - 修正前
    ```
            $.ajax($searchInput.data('offline-search-index-json-src')).then(
                (data) => {
                    idx = lunr(function () {
                        this.ref('ref');
    
                        // If you added more searchable fields to the search index, list them here.
                        // Here you can specify searchable fields to the search index - e.g. individual toxonomies for you project
                        // With "boost" you can add weighting for specific (default weighting without boost: 1)
                        this.field('title', { boost: 5 });
                        this.field('categories', { boost: 3 });
                        this.field('tags', { boost: 3 });
                        // this.field('projects', { boost: 3 }); // example for an individual toxonomy called projects
                        this.field('description', { boost: 2 });
                        this.field('body');
    
                        data.forEach((doc) => {
                            this.add(doc);
    
                            resultDetails.set(doc.ref, {
                                title: doc.title,
                                excerpt: doc.excerpt,
                            });
                        });
                    });
    
                    $searchInput.trigger('change');
                }
            );
    ```
  - 修正後
    ```
            $.ajax($searchInput.data('offline-search-index-json-src')).then(
                (data) => {
                    idx = lunr(function () {
                        this.ref('ref');
    
                        // If you added more searchable fields to the search index, list them here.
                        // Here you can specify searchable fields to the search index - e.g. individual toxonomies for you project
                        // With "boost" you can add weighting for specific (default weighting without boost: 1)
                        this.field('title', { boost: 5 });
                        this.field('categories', { boost: 3 });
                        this.field('tags', { boost: 3 });
                        // this.field('projects', { boost: 3 }); // example for an individual toxonomy called projects
                        this.field('description', { boost: 2 });
                        this.field('body');
                        // ★ 日本語追加
                        this.use(lunr.ja);
    
                        data.forEach((doc) => {
                            this.add(doc);
    
                            resultDetails.set(doc.ref, {
                                title: doc.title,
                                excerpt: doc.excerpt,
                            });
                        });
                    });
    
                    $searchInput.trigger('change');
                }
            );
    ```
- `themes/docsy/layouts/partials/head.html` を `layouts/partials/head.html` にコピーして修正
  - 修正前
    ```
    {{ if .Site.Params.offlineSearch }}
    <script
      src="https://unpkg.com/lunr@2.3.8/lunr.min.js"
      integrity="sha384-vRQ9bDyE0Wnu+lMfm57BlYLO0/XauFuKpVsZPs7KEDwYKktWi5+Kz3MP8++DFlRY"
      crossorigin="anonymous"></script>
    {{end}}
    ```
  - 修正後
    ```
    {{ if .Site.Params.offlineSearch }}
    <script
      src="https://unpkg.com/lunr@2.3.8/lunr.min.js"
      integrity="sha384-vRQ9bDyE0Wnu+lMfm57BlYLO0/XauFuKpVsZPs7KEDwYKktWi5+Kz3MP8++DFlRY"
      crossorigin="anonymous"></script>
      <!-- ★ 日本語追加 -->
      <script src="https://cdn.jsdelivr.net/npm/lunr-languages@1.9.0/lunr.stemmer.support.js" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/lunr-languages@1.9.0/tinyseg.js" crossorigin="anonymous"></script>
      <script src="https://cdn.jsdelivr.net/npm/lunr-languages@1.9.0/lunr.ja.js" crossorigin="anonymous"></script>
    {{end}}
    ```
